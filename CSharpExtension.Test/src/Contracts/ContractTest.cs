﻿using System;
using NUnit.Framework;

namespace CSharpExtension.Test.Contracts
{
    [TestFixture]
    public class ContractTest
    {
        [Test]
        public void RequiresArgumentNotNullSuccess()
        {
            var s = string.Empty;
            //
            Contract.RequiresArgumentNotNull(s);
        }

        [Test]
        public void RequiresArgumentNotNullFailure()
        {
            string s = null;
            //
            var ex = Assert.Catch<ArgumentNullException>(() => Contract.RequiresArgumentNotNull(s));
            Assert.AreEqual("Значение не может быть неопределенным.", ex.Message);
        }

        [Test]
        public void RequiresArgumentNotNullFailureWithMessage()
        {
            string s = null;
            //
            var ex = Assert.Catch<ArgumentNullException>(() => Contract.RequiresArgumentNotNull(s, nameof(s)));
            Assert.AreEqual($"Значение не может быть неопределенным.\r\nИмя параметра: {nameof(s)}", ex.Message);
        }

        [Test]
        public void RequiresNotNullSuccess()
        {
            var s = string.Empty;
            //
            Contract.RequiresNotNull(s);
        }

        [Test]
        public void RequiresNotNullFailure()
        {
            string s = null;
            //
            var ex = Assert.Catch<NullReferenceException>(() => Contract.RequiresNotNull(s));
            Assert.AreEqual("Ссылка на объект не указывает на экземпляр объекта.", ex.Message);
        }

        [Test]
        public void RequiresNotNullFailureWithMessage()
        {
            string s = null;
            //
            var ex = Assert.Catch<NullReferenceException>(() => Contract.RequiresNotNull(s, nameof(s)));
            Assert.AreEqual(nameof(s), ex.Message);
        }
    }
}
