﻿using System;
using CSharpExtension.DateTimes;
using FluentAssertions;
using NUnit.Framework;

namespace CSharpExtension.Test.DateTimes
{
    [TestFixture]
    public class DateTimeExtensionTest
    {
        public DateTime Time = new DateTime(2008,11,1,11,11,11);

        [Test]
        public void TrimSeconds()
        {
            var t = Time.TrimSeconds();
            //
            t.Second.Should().Be(0);
            t.Minute.Should().NotBe(0);
            t.Hour.Should().NotBe(0);
        }

        [Test]
        public void TrimMinutes()
        {
            var t = Time.TrimMinutes();
            //
            t.Second.Should().Be(0);
            t.Minute.Should().Be(0);
            t.Hour.Should().NotBe(0);
        }

        [Test]
        public void TrimHours()
        {
            var t = Time.TrimHours();
            //
            t.Second.Should().Be(0);
            t.Minute.Should().Be(0);
            t.Hour.Should().Be(0);
        }

        [Test]
        public void TrimHours1()
        {
            var t = Time.TrimHours();
            //
            t.Second.Should().Be(0);
            t.Minute.Should().Be(0);
            t.Hour.Should().Be(0);
        }
    }
}
