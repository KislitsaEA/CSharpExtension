﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using NUnit.Framework;


namespace CSharpExtension.Test
{
    [TestFixture]
    public class DoubleDecimalDelimeterParseTesr
    {
        [Test]
        public void Tst1()
        {
            const string str = "0.5";
            double d;
            double.TryParse(str,out d);
            Assert.AreNotEqual(0.5,d);
        }

        [Test]
        public void Tst2()
        {
            const string str = "0,5";
            double d;
            double.TryParse(str,out d);
            Assert.AreEqual(0.5, d);
        }

        [Test]
        public void Tst11()
        {
            const string str = "0.5";
            double d;
            double.TryParse(str,NumberStyles.Any, CultureInfo.InvariantCulture,out d);
            Assert.AreEqual(0.5, d);
        }

        [Test]
        public void Tst12()
        {
            const string str = "0,5";
            double d;
            double.TryParse(str, NumberStyles.Any, CultureInfo.CurrentCulture, out d);
            Assert.AreEqual(0.5, d);
        }

        [Test]
        public void Tst4()
        {
            const string str = "0,5";
            double d;
            double.TryParse(str, out d);
            Assert.AreEqual(0.5, d);
        }
    }
}
