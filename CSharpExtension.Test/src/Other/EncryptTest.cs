﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace CSharpExtension.Test
{
    [TestFixture]
    public class EncryptTest
    {
        [TestCase("", "xk5yR6XwjArb4VrPp9PCEA==")]
        [TestCase("a", "qNf4RJbnVMwnwpXSs9PSrw==")]
        [TestCase("ae", "xJoiAa6LeUXDvBgoJBuVtg==")]
        [TestCase("act", "79+PP4kGP4JOmp2m4ulMNA==")]
        [TestCase("zxve", "lbKxweZL3i5t/vfkKXeean+F/CqVBllAvkX8tLV33QA=")]
        [TestCase("hfcsr", "HPkLJnJ3uvxQ9l7SxxDjFt4UEIgMq96nIWJE4AMX81g=")]
        [TestCase("jgddtf", "b38SpVqDTl4aXwFRxoFyVR06SE98RSzWxGMLWFue8M4=")]
        [TestCase("edfhuyt", "BEmbdbda7/Y22wYM8OakCWYm7So5cA0oUMvaznQSNSg=")]
        [TestCase("edfgcvcx", "v9fYbiP/1ieZm9Ap7BrK6xid7NWR9xZ0JpzHlcJaQPk=")]
        [TestCase("rfvhgdfsw", "2nn0aS68TFAfa7iPD734pusDa2YPGDeyNN5Y1FPZpOI=")]
        [TestCase("edfvgtrdxf", "O41V/z/UF4nez8oc1/MwNzrEqhRTbiyM3BfUH1oXwqw=")]
        public void EncryptString(string pass, string result)
        {
            var encryptString = Encrypt.EncryptString(pass);

            Assert.AreEqual(result,encryptString);
        }

        [TestCase("", "xk5yR6XwjArb4VrPp9PCEA==")]
        [TestCase("a", "qNf4RJbnVMwnwpXSs9PSrw==")]
        [TestCase("ae", "xJoiAa6LeUXDvBgoJBuVtg==")]
        [TestCase("act", "79+PP4kGP4JOmp2m4ulMNA==")]
        [TestCase("zxve", "lbKxweZL3i5t/vfkKXeean+F/CqVBllAvkX8tLV33QA=")]
        [TestCase("hfcsr", "HPkLJnJ3uvxQ9l7SxxDjFt4UEIgMq96nIWJE4AMX81g=")]
        [TestCase("jgddtf", "b38SpVqDTl4aXwFRxoFyVR06SE98RSzWxGMLWFue8M4=")]
        [TestCase("edfhuyt", "BEmbdbda7/Y22wYM8OakCWYm7So5cA0oUMvaznQSNSg=")]
        [TestCase("edfgcvcx", "v9fYbiP/1ieZm9Ap7BrK6xid7NWR9xZ0JpzHlcJaQPk=")]
        [TestCase("rfvhgdfsw", "2nn0aS68TFAfa7iPD734pusDa2YPGDeyNN5Y1FPZpOI=")]
        [TestCase("edfvgtrdxf", "O41V/z/UF4nez8oc1/MwNzrEqhRTbiyM3BfUH1oXwqw=")]
        public void DecryptString(string pass, string result)
        {
            var encryptString = Encrypt.DecryptString(result);

            Assert.AreEqual(pass, encryptString);
        }

        [TestCase("", "xk5yR6XwjArb4VrPp9PCEA==")]
        [TestCase("a", "qNf4RJbnVMwnwpXSs9PSrw==")]
        [TestCase("ae", "xJoiAa6LeUXDvBgoJBuVtg==")]
        [TestCase("act", "79+PP4kGP4JOmp2m4ulMNA==")]
        [TestCase("zxve", "lbKxweZL3i5t/vfkKXeean+F/CqVBllAvkX8tLV33QA=")]
        [TestCase("hfcsr", "HPkLJnJ3uvxQ9l7SxxDjFt4UEIgMq96nIWJE4AMX81g=")]
        [TestCase("jgddtf", "b38SpVqDTl4aXwFRxoFyVR06SE98RSzWxGMLWFue8M4=")]
        [TestCase("edfhuyt", "BEmbdbda7/Y22wYM8OakCWYm7So5cA0oUMvaznQSNSg=")]
        [TestCase("edfgcvcx", "v9fYbiP/1ieZm9Ap7BrK6xid7NWR9xZ0JpzHlcJaQPk=")]
        [TestCase("rfvhgdfsw", "2nn0aS68TFAfa7iPD734pusDa2YPGDeyNN5Y1FPZpOI=")]
        [TestCase("edfvgtrdxf", "O41V/z/UF4nez8oc1/MwNzrEqhRTbiyM3BfUH1oXwqw=")]
        public void IsEncrypted_True(string pass, string result)
        {
            var isEncr = Encrypt.IsEncrypted(result);

            Assert.IsTrue(isEncr);
        }

        [TestCase("", "xk5yR6XwjArb4VrPp9PCEA==")]
        [TestCase("a", "qNf4RJbnVMwnwpXSs9PSrw==")]
        [TestCase("ae", "xJoiAa6LeUXDvBgoJBuVtg==")]
        [TestCase("act", "79+PP4kGP4JOmp2m4ulMNA==")]
        [TestCase("zxve", "lbKxweZL3i5t/vfkKXeean+F/CqVBllAvkX8tLV33QA=")]
        [TestCase("hfcsr", "HPkLJnJ3uvxQ9l7SxxDjFt4UEIgMq96nIWJE4AMX81g=")]
        [TestCase("jgddtf", "b38SpVqDTl4aXwFRxoFyVR06SE98RSzWxGMLWFue8M4=")]
        [TestCase("edfhuyt", "BEmbdbda7/Y22wYM8OakCWYm7So5cA0oUMvaznQSNSg=")]
        [TestCase("edfgcvcx", "v9fYbiP/1ieZm9Ap7BrK6xid7NWR9xZ0JpzHlcJaQPk=")]
        [TestCase("rfvhgdfsw", "2nn0aS68TFAfa7iPD734pusDa2YPGDeyNN5Y1FPZpOI=")]
        [TestCase("edfvgtrdxf", "O41V/z/UF4nez8oc1/MwNzrEqhRTbiyM3BfUH1oXwqw=")]
        public void IsEncrypted_False(string pass, string result)
        {
            var isEncr = Encrypt.IsEncrypted(pass);

            Assert.IsFalse(isEncr);
        }

    }
}
