using System.Collections.Generic;
using System.Linq;
using CSharpExtension.Fronts;
using NUnit.Framework;

namespace CSharpExtension.Test
{
    [TestFixture]
    public class FrontCacherUnitTest
    {
        [Test]
        public void Test1()
        {
            var receivedEvents = new List<FrontEventArgs>();

            var directFrontEventArg = new FrontEventArgs()
            {
                Message = "������ �����"
            };

            var backFrontEventArg = new FrontEventArgs()
            {
                Message = "�������� �����"
            };

            var cacher = new FrontCacher<int>(10, (settings, argumet) => argumet > settings, directFrontEventArg, backFrontEventArg);

            cacher.DirectFront += delegate(object sender, FrontEventArgs e)
            {
                receivedEvents.Add(e);
            };

            cacher.BackFront += delegate (object sender, FrontEventArgs e)
            {
                receivedEvents.Add(e);
            };
            
            cacher.CheckValue(1);
            Assert.AreEqual(false,cacher.Front);
            Assert.AreEqual(0, receivedEvents.Count);
            Assert.AreEqual(null, receivedEvents.LastOrDefault());
            

            cacher.CheckValue(11);
            Assert.AreEqual(true, cacher.Front);
            Assert.AreEqual(1, receivedEvents.Count);
            Assert.AreEqual(directFrontEventArg, receivedEvents.LastOrDefault());

            cacher.CheckValue(15);
            Assert.AreEqual(true, cacher.Front);
            Assert.AreEqual(1, receivedEvents.Count);
            Assert.AreEqual(directFrontEventArg, receivedEvents.LastOrDefault());

            cacher.CheckValue(8);
            Assert.AreEqual(false, cacher.Front);
            Assert.AreEqual(2, receivedEvents.Count);
            Assert.AreEqual(backFrontEventArg, receivedEvents.LastOrDefault());

        }

        private void Cacher_BackFront(object sender, FrontEventArgs e)
        {
            //throw new NotImplementedException();
        }

        private void Cacher_DirectFront(object sender, FrontEventArgs e)
        {
            //throw new NotImplementedException();
        }
    }
}