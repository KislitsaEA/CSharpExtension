﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;


namespace CSharpExtension.Test
{
    [TestFixture]
    public class GenericExtensionIsInTest
    {
        [Test]
        public void Test1()
        {
            Assert.IsTrue(1.IsIn(1,2,3));
        }

        [Test]
        public void Test2()
        {
            Assert.IsTrue(!10.IsIn(1, 2, 3));
        }

        [Test]
        public void Test3()
        {
            Assert.IsTrue(1.0.IsIn(1, 2, 3));
        }

        [Test]
        public void Test4()
        {
            Assert.IsTrue(!10.0.IsIn(1, 2.5, 3));
        }

        [Test]
        public void IsInRange1()
        {
            Assert.IsTrue(5.IsInRange(0,10));
        }

        [Test]
        public void IsInRange2()
        {
            Assert.IsFalse(0.IsInRange(0, 10));
        }

        [Test]
        public void IsInRange3()
        {
            Assert.IsFalse(10.IsInRange(0, 10));
        }

        [Test]
        public void IsInRangeEqualMin1()
        {
            Assert.IsTrue(5.IsInRangeEqualMin(0, 10));
        }

        [Test]
        public void IsInRangeEqualMin2()
        {
            Assert.IsTrue(0.IsInRangeEqualMin(0, 10));
        }

        [Test]
        public void IsInRangeEqualMin3()
        {
            Assert.IsFalse(10.IsInRangeEqualMin(0, 10));
        }

        [Test]
        public void IsInRangeEqualMax1()
        {
            Assert.IsTrue(5.IsInRangeEqualMax(0, 10));
        }

        [Test]
        public void IsInRangeEqualMax2()
        {
            Assert.IsFalse(0.IsInRangeEqualMax(0, 10));
        }

        [Test]
        public void IsInRangeEqualMax3()
        {
            Assert.IsTrue(10.IsInRangeEqualMax(0, 10));
        }

        [Test]
        public void IsInEqualRange1()
        {
            Assert.IsTrue(5.IsInEqualRange(0, 10));
        }

        [Test]
        public void IsInEqualRange2()
        {
            Assert.IsTrue(0.IsInEqualRange(0, 10));
        }

        [Test]
        public void IsInEqualRange3()
        {
            Assert.IsTrue(10.IsInEqualRange(0, 10));
        }
    }
}
