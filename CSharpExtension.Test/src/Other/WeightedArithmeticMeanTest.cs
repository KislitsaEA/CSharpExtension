﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSharpExtension.IEnumerableExtensions;
using NUnit.Framework;

namespace CSharpExtension.Test
{
    [TestFixture]
    public class WeightedArithmeticMeanTest
    {
        [Test]
        public void Test1()
        {
            var k = new Dictionary<double, double>() { {1,1}, {2,1}, {3,1} };
            var r = k.WeightedArithmeticMean(v => v.Key, w => w.Value);
            Assert.AreEqual(2,r);
        }

        [Test]
        public void Test2()
        {
            var k = new Dictionary<double, double>() { { 1, 1 }, { 2, 1 }, { 3, 1 } };
            var r = k.WeightedArithmeticMean(v => v.Key, w => w.Value);
            Assert.AreEqual(2, r);
        }
    }
}
