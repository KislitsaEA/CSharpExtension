using System.IO;
using System.Text;
using CSharpExtension.Serialization;
using NUnit.Framework;

namespace CSharpExtension.Test.Serialization
{
    [TestFixture]
    public class JsonSerializerTest
    {
        private string _serializedObject;
        private SimpleClass _object;
        private readonly JsonSerializer _jsonSerializer = new JsonSerializer();

        [SetUp]
        public void SetUp()
        {
            _object = new SimpleClass()
            {
                IntValue = 5,
                StringValue = "five"
            };

            var sb = new StringBuilder();
            sb.AppendLine("{");
            sb.AppendLine("  \"StringValue\": \"five\",");
            sb.AppendLine("  \"IntValue\": 5");
            sb.Append("}");
            _serializedObject = sb.ToString();
        }

        [Test]
        public void Serialize()
        {
            var str = _jsonSerializer.Serialize(_object);
            //
            Assert.AreEqual(_serializedObject,str);
        }

        [Test]
        public void Deserialize()
        {
            var obj = _jsonSerializer.Deserialize<SimpleClass>(_serializedObject);
            //
            Assert.AreEqual(_object, obj);
        }

        [Test]
        public void DeserializeFromFile()
        {
            var filePath = Path.Combine(AssemblyPathHelper.GetCurrentAssemblyPath,"src\\Serialization\\Data\\SimpleClassObject.json");
            var fileInfo = new FileInfo(filePath);
            //
            var obj = _jsonSerializer.DeserializeFtomFile<SimpleClass>(fileInfo);
            //
            Assert.AreEqual(_object, obj);
        }

    }
}