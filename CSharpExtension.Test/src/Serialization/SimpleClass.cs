using System;

namespace CSharpExtension.Test.Serialization
{
    public class SimpleClass : IEquatable<SimpleClass>
    {
        public string StringValue { get; set; }
        public int IntValue { get; set; }

        public bool Equals(SimpleClass other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return string.Equals(StringValue, other.StringValue) && IntValue == other.IntValue;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((SimpleClass)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((StringValue != null ? StringValue.GetHashCode() : 0) * 397) ^ IntValue;
            }
        }

        public static bool operator ==(SimpleClass left, SimpleClass right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(SimpleClass left, SimpleClass right)
        {
            return !Equals(left, right);
        }
    }
}