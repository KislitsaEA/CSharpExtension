﻿using CSharpExtension.StringExtension;
using NUnit.Framework;

namespace CSharpExtension.Test.StringExtension
{
    [TestFixture]
    public class ParsecStringExtensionTest
    {
        [TestCase("Zero", TestEnum.Zero)]
        [TestCase("One", TestEnum.One)]
        [TestCase("Two", TestEnum.Two)]
        public void To_T_ByNameTest(string str, TestEnum requare)
        {
            var val = str.ToEnum<TestEnum>();
            //
            Assert.IsTrue(val.HasValue);
            Assert.AreEqual(val.Value, requare);
        }

        [TestCase("0", TestEnum.Zero)]
        [TestCase("1", TestEnum.One)]
        [TestCase("2", TestEnum.Two)]
        public void To_T_ByNumberAsStringTest(string str, TestEnum requare)
        {
            var val = str.ToEnum<TestEnum>();
            //
            Assert.IsTrue(val.HasValue);
            Assert.AreEqual(val.Value, requare);
        }
    
        public enum TestEnum
        {
            Zero = 0,
            One = 1,
            Two = 2
        }
    }
}