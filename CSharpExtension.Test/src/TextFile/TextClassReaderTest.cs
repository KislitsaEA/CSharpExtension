﻿using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSharpExtension.FileReaders;
using CSharpExtension.StringExtension;
using NUnit.Framework;
using NUnit.Framework.Internal;

namespace CSharpExtension.Test.TextFile
{
    [TestFixture]
    public class TextClassReaderTest
    {
        //private readonly string _path = @"/Content/TextFile.txt";
        private ITextFileReader _fileReader;
        private string _fileContent;


        [SetUp]
        public void SetUp()
        {
            var path = Path.Combine(AssemblyPathHelper.GetCurrentAssemblyPath, "Content", "TextFile.txt");
            _fileReader = new TextFileReader(path);
            var sb = new StringBuilder();
            sb.AppendLine("Aaaa").AppendLine("Bbbb").AppendLine().Append("Cccc");
            _fileContent = sb.ToString();
            //_fileContent = "Aaaa\nBbbb\n\nCccc";
        }

        [TearDown]
        public void TearDown()
        {

        }

        [Test]
        public void ReadToEndTest()
        {
            var str = _fileReader.ReadToEnd();
            str = string.Join("\r\n", str.SplitByLines());
            //
            Assert.AreEqual(_fileContent, str);
        }

        [Test]
        public void ReadLinesTest()
        {
            var lines = _fileContent.SplitByLines();
            //
            var strs = _fileReader.ReadLines().ToArray();
            //
            Assert.AreEqual(lines.Length, strs.Length);
            for (var i = 0; i < lines.Length; i++)
            {
                Assert.AreEqual(lines[i], strs[i]);
            }
        }


        [Test]
        public async Task ReadToEndTestAsync()
        {
            var str = await _fileReader.ReadToEndAsync();
            str = string.Join("\r\n", str.SplitByLines());
            //
            Assert.AreEqual(_fileContent, str);
        }

        [Test]
        public async Task ReadLinesTestAsync()
        {
            var lines = _fileContent.SplitByLines();
            //
            var strs = (await _fileReader.ReadLinesAsync()).ToArray();
            //
            Assert.AreEqual(lines.Length, strs.Length);
            for (var i = 0; i < lines.Length; i++)
            {
                Assert.AreEqual(lines[i], strs[i]);
            }
        }
    }
}