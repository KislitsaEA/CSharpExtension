﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;

namespace CSharpExtension.Test.src.TypeCaster
{
    [TestFixture]
    public class CastExtensionTest
    {
        [Test]
        public void Object_To_Int()
        {
            int expected = 12345;
            object obj = (object)expected;
            //
            int value = obj.CastTo<int>();
            //
            value.Should().BeOfType(typeof(int));
            value.Should().Be(expected);
        }

        [Test]
        public void Object_To_Double()
        {
            double expected = 12345.5;
            object obj = (object)expected;
            //
            double value = obj.CastTo<double>();
            //
            value.Should().BeOfType(typeof(double));
            value.Should().Be(expected);
        }

        [Test]
        public void Int_To_Double()
        {
            int expected = 12345;
            //
            double value = expected.CastTo<double>();
            //
            value.Should().BeOfType(typeof(double));
            value.Should().Be(expected);
        }

        [Test]
        public void DateTime_To_TestDateTime()
        {
            DateTime expected = DateTime.Now;
            //
            var value = expected.CastTo<TestDateTime>();
            //
            value.Should().BeOfType(typeof(TestDateTime));
            value.Time.Should().Be(expected);
        }

        [Test]
        public void TestDateTime_To_DateTime()
        {
            var expected = new TestDateTime(DateTime.Now);
            //
            var value = expected.CastTo<DateTime>();
            //
            Assert.IsInstanceOf<DateTime>(value);
            value.Should().Be(expected.Time);
        }

        [Test]
        public void Cast_Int_To_Double()
        {
            int i = 100;
            double d = i.CastTo<double>();
            Assert.AreEqual(100.0, d);
        }

        [Test]
        public void Cast_Double_To_Int()
        {
            double d = 100.0;
            int i = d.CastTo<int>();
            Assert.AreEqual(100, i);
        }

        [Test]
        public void Cast_Double_To_Int1()
        {
            double d = 100.1;
            int i = d.CastTo<int>();
            Assert.AreEqual(100, i);
        }

        [Test]
        public void Cast_Double_To_Int2()
        {
            double d = 100.5;
            int i = d.CastTo<int>();
            Assert.AreEqual(100, i);
        }

        [Test]
        public void Cast_Double_To_Int3()
        {
            double d = 100.6;
            int i = d.CastTo<int>();
            Assert.AreEqual(101, i);
        }


        [Test]
        public void Cast_String_To_Int()
        {
            string str = "100";
            int i = str.CastTo<int>();
            Assert.AreEqual(100, i);
        }

        [Test]
        public void Cast_Int_To_Str()
        {
            int i = 100;
            string str = i.CastTo<string>();
            Assert.AreEqual("100", str);
        }


        [Test]
        public void Cast_String_To_Double1()
        {
            var i = "1,2";
            double d = i.CastTo<double>();
            Assert.AreEqual(1.2, d);
        }

        [Test]
        public void Cast_String_To_Double2()
        {
            var i = "1.2";
            double d = i.CastTo<double>();
            Assert.AreEqual(1.2, d);
        }


        [Test]
        public void Cast_String_To_Double3()
        {
            var i = "1.2";
            double d = i.CastTo<double>();
            Assert.AreEqual(1.2, d);
        }

        [Test]
        public void Test1()
        {
            var i = 9;
            var o = (object)i;

            var j = i.CastTo<int>();

            Assert.AreEqual(i, j);
        }

        [Test]
        public void Test2()
        {
            var i = 9;
            var o = (object)i;

            var j = i.CastTo<double>();

            Assert.AreEqual(i, j);
        }



        [Test]
        public void Test3()
        {
            var i = ConsoleColor.Blue;
            var o = (object)i;

            var j = i.CastTo<ConsoleColor>();

            Assert.AreEqual(i, j);
        }



        [Test]
        public void Test4()
        {
            var i = "Blue";
            var o = (object)i;

            var j = i.CastTo<ConsoleColor>();

            Assert.AreEqual(ConsoleColor.Blue, j);
        }

        [Test]
        public void Test5()
        {
            var i = ConsoleColor.Blue;
            var o = (int)i;

            var j = i.CastTo<ConsoleColor>();

            Assert.AreEqual(i, j);
        }


        [Test]
        public void ParseDouble_Test_1()
        {
            var i = "1.25";
            var k = 1.25;

            var j = i.CastTo<double>();

            Assert.AreEqual(k, j);
        }

        [Test]
        public void ParseDouble_Test_2()
        {
            var i = "1,25";
            var k = 1.25;

            var j = i.CastTo<double>();

            Assert.AreEqual(k, j);
        }


        [Test]
        public void ParseDouble_Test_3()
        {
            var i = "1 123,25";
            var k = 1123.25;

            var j = i.CastTo<double>();

            Assert.AreEqual(k, j);
        }

        [Test]
        public void ParseDouble_Test_4()
        {
            var i = "1 123.25";
            var k = 1123.25;

            var j = i.CastTo<double>();

            Assert.AreEqual(k, j);
        }

        [Test]
        public void ParseDouble_Test_5()
        {
            var i = "1.123,25";
            var k = 1123.25;

            var j = i.CastTo<double>();

            Assert.AreEqual(k, j);
        }

        [Test]
        public void ParseDouble_Test__1()
        {
            var i = "1.234.567,89";
            var k = 1234567.89;

            var j = i.CastTo<double>();

            Assert.AreEqual(k, j);
        }

        [Test]
        public void ParseDouble_Test__2()
        {
            var i = "1 234 567,89";
            var k = 1234567.89;

            var j = i.CastTo<double>();

            Assert.AreEqual(k, j);
        }

        [Test]
        public void ParseDouble_Test__3()
        {
            var i = "1 234 567.89";
            var k = 1234567.89;

            var j = i.CastTo<double>();

            Assert.AreEqual(k, j);
        }

        [Test]
        public void ParseDouble_Test__4()
        {
            var i = "1,234,567.89";
            var k = 1234567.89;

            var j = i.CastTo<double>();

            Assert.AreEqual(k, j);
        }

        [Test]
        public void ParseDouble_Test__5()
        {
            var i = "123456789";
            var k = 123456789;

            var j = i.CastTo<double>();

            Assert.AreEqual(k, j);
        }

        [Test]
        public void ParseDouble_Test__6()
        {
            var i = "1234567,89";
            var k = 1234567.89;

            var j = i.CastTo<double>();

            Assert.AreEqual(k, j);
        }

        [Test]
        public void ParseDouble_Test__7()
        {
            var i = "1234567.89";
            var k = 1234567.89;

            var j = i.CastTo<double>();

            Assert.AreEqual(k, j);
        }


        [Test]
        public void ParseSingle_Test_1()
        {
            var i = "1.25";
            var k = 1.25;

            var j = i.CastTo<float>();

            Assert.AreEqual(k, j);
        }

        [Test]
        public void ParseSingle_Test_2()
        {
            var i = "1,25";
            var k = 1.25;

            var j = i.CastTo<float>();

            Assert.AreEqual(k, j);
        }

        [Test]
        public void ParseInt_Test()
        {
            var i = "125";
            var k = 125;

            var j = i.CastTo<int>();

            Assert.AreEqual(k, j);
        }

        [Test]
        public void ParseShort_Test()
        {
            var i = "125";
            var k = 125;

            var j = i.CastTo<short>();

            Assert.AreEqual(k, j);
        }

        [Test]
        public void CastIntToDouble()
        {
            int i = 100;
            double d = i.CastTo<double>();
            Assert.AreEqual(100.0, d);
        }

        [Test]
        public void CastDoubleToInt()
        {
            double d = 100.0;
            int i = d.CastTo<int>();
            Assert.AreEqual(100, i);
        }

        [Test]
        public void CastDoubleToInt1()
        {
            double d = 100.1;
            int i = d.CastTo<int>();
            Assert.AreEqual(100, i);
        }

        [Test]
        public void CastDoubleToInt2()
        {
            double d = 100.5;
            int i = d.CastTo<int>();
            Assert.AreEqual(100, i);
        }

        [Test]
        public void CastDoubleToInt3()
        {
            double d = 100.6;
            int i = d.CastTo<int>();
            Assert.AreEqual(101, i);
        }

        [Test]
        public void CastStringToInt()
        {
            string str = "100";
            int i = str.CastTo<int>();
            Assert.AreEqual(100, i);
        }

        [Test]
        public void CastIntToStr()
        {
            int i = 100;
            string str = i.CastTo<string>();
            Assert.AreEqual("100", str);
        }


        [Test]
        public void CastStringToDouble1()
        {
            var i = "1,2";
            double d = i.CastTo<double>();
            Assert.AreEqual(1.2, d);
        }

        [Test]
        public void CastStringToDouble2()
        {
            var i = "1.2";
            double d = i.CastTo<double>();
            Assert.AreEqual(1.2, d);
        }

        [Test]
        public void CastDBNullToStr()
        {
            object i = (object)DBNull.Value;
            string str = i.CastTo<string>();
            Assert.AreEqual("", str);
        }

        [Test]
        public void CastDBNullToIntFail()
        {
            var i = (object) DBNull.Value;
            Assert.Throws<InvalidCastException>(() => i.CastTo<int>());
        }
    }
}
