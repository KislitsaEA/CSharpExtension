﻿using System;

namespace CSharpExtension.Test.src.TypeCaster
{
    public struct TestDateTime
    {
        public TestDateTime(DateTime time)
        {
            Time = time;
        }

        public DateTime Time { get; private set; }

        /// <summary>
        /// Явное приведение Timestamp => DateTime
        /// </summary>
        /// <param name="timestamp"></param>
        public static explicit operator DateTime(TestDateTime timestamp)
        {
            return timestamp.Time;
        }

        /// <summary>
        /// Явное приведение DateTime => Timestamp
        /// </summary>
        /// <param name="time"></param>
        public static explicit operator TestDateTime(DateTime time)
        {
            return new TestDateTime(time);
        }
    }
}