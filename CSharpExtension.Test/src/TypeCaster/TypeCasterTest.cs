﻿

using System;
using NUnit.Framework;

namespace CSharpExtension.Test
{
    [TestFixture]
    public class TypeCasterTest
    {
        [Test]
        public void Test1()
        {
            var i = 9;
            var o = (object)i;

            var j = TypeCaster.Cast<int>(o);

            Assert.AreEqual(i, j);
        }

        [Test]
        public void Test2()
        {
            var i = 9;
            var o = (object)i;

            var j = TypeCaster.Cast<double>(o);

            Assert.AreEqual(i, j);
        }



        [Test]
        public void Test3()
        {
            var i = ConsoleColor.Blue;
            var o = (object)i;

            var j = TypeCaster.Cast<ConsoleColor>(o);

            Assert.AreEqual(i, j);
        }



        [Test]
        public void Test4()
        {
            var i = "Blue";
            var o = (object)i;

            var j = TypeCaster.Cast<ConsoleColor>(o);

            Assert.AreEqual(ConsoleColor.Blue, j);
        }

        [Test]
        public void Test5()
        {
            var i = ConsoleColor.Blue;
            var o = (int)i;

            var j = TypeCaster.Cast<ConsoleColor>(o);

            Assert.AreEqual(i, j);
        }


        [Test]
        public void ParseDouble_Test_1()
        {
            var i = "1.25";
            var k = 1.25;

            var j = TypeCaster.Cast<double>(i);

            Assert.AreEqual(k, j);
        }

        [Test]
        public void ParseDouble_Test_2()
        {
            var i = "1,25";
            var k = 1.25;

            var j = TypeCaster.Cast<double>(i);

            Assert.AreEqual(k, j);
        }


        [Test]
        public void ParseDouble_Test_3()
        {
            var i = "1 123,25";
            var k = 1123.25;

            var j = TypeCaster.Cast<double>(i);

            Assert.AreEqual(k, j);
        }

        [Test]
        public void ParseDouble_Test_4()
        {
            var i = "1 123.25";
            var k = 1123.25;

            var j = TypeCaster.Cast<double>(i);

            Assert.AreEqual(k, j);
        }

        [Test]
        public void ParseDouble_Test_5()
        {
            var i = "1.123,25";
            var k = 1123.25;

            var j = TypeCaster.Cast<double>(i);

            Assert.AreEqual(k, j);
        }

        [Test]
        public void ParseDouble_Test__1()
        {
            var i = "1.234.567,89";
            var k = 1234567.89;

            var j = TypeCaster.Cast<double>(i);

            Assert.AreEqual(k, j);
        }

        [Test]
        public void ParseDouble_Test__2()
        {
            var i = "1 234 567,89";
            var k = 1234567.89;

            var j = TypeCaster.Cast<double>(i);

            Assert.AreEqual(k, j);
        }

        [Test]
        public void ParseDouble_Test__3()
        {
            var i = "1 234 567.89";
            var k = 1234567.89;

            var j = TypeCaster.Cast<double>(i);

            Assert.AreEqual(k, j);
        }

        [Test]
        public void ParseDouble_Test__4()
        {
            var i = "1,234,567.89";
            var k = 1234567.89;

            var j = TypeCaster.Cast<double>(i);

            Assert.AreEqual(k, j);
        }

        [Test]
        public void ParseDouble_Test__5()
        {
            var i = "123456789";
            var k = 123456789;

            var j = TypeCaster.Cast<double>(i);

            Assert.AreEqual(k, j);
        }

        [Test]
        public void ParseDouble_Test__6()
        {
            var i = "1234567,89";
            var k = 1234567.89;

            var j = TypeCaster.Cast<double>(i);

            Assert.AreEqual(k, j);
        }

        [Test]
        public void ParseDouble_Test__7()
        {
            var i = "1234567.89";
            var k = 1234567.89;

            var j = TypeCaster.Cast<double>(i);

            Assert.AreEqual(k, j);
        }


        [Test]
        public void ParseSingle_Test_1()
        {
            var i = "1.25";
            var k = 1.25;

            var j = TypeCaster.Cast<float>(i);

            Assert.AreEqual(k, j);
        }

        [Test]
        public void ParseSingle_Test_2()
        {
            var i = "1,25";
            var k = 1.25;

            var j = TypeCaster.Cast<float>(i);

            Assert.AreEqual(k, j);
        }

        [Test]
        public void ParseInt_Test()
        {
            var i = "125";
            var k = 125;

            var j = TypeCaster.Cast<int>(i);

            Assert.AreEqual(k, j);
        }

        [Test]
        public void ParseShort_Test()
        {
            var i = "125";
            var k = 125;

            var j = TypeCaster.Cast<short>(i);

            Assert.AreEqual(k, j);
        }
    }
}
