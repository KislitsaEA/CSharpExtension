

using NUnit.Framework;

namespace CSharpExtension.Test
{
    [TestFixture]
    public class TypeCasterTest1
    {
        [Test]
        public void CastIntToDouble()
        {
            int i = 100;
            double d = TypeCaster.Cast<double>(i);
            Assert.AreEqual(100.0, d);
        }

        [Test]
        public void CastDoubleToInt()
        {
            double d = 100.0;
            int i = TypeCaster.Cast<int>(d);
            Assert.AreEqual(100, i);
        }

        [Test]
        public void CastDoubleToInt1()
        {
            double d = 100.1;
            int i = TypeCaster.Cast<int>(d);
            Assert.AreEqual(100, i);
        }

        [Test]
        public void CastDoubleToInt2()
        {
            double d = 100.5;
            int i = TypeCaster.Cast<int>(d);
            Assert.AreEqual(100, i);
        }

        [Test]
        public void CastDoubleToInt3()
        {
            double d = 100.6;
            int i = TypeCaster.Cast<int>(d);
            Assert.AreEqual(101, i);
        }


        [Test]
        public void CastStringToInt()
        {
            string str = "100";
            int i = TypeCaster.Cast<int>(str);
            Assert.AreEqual(100, i);
        }

        [Test]
        public void CastIntToStr()
        {
            int i = 100;
            string str = TypeCaster.Cast<string>(i);
            Assert.AreEqual("100", str);
        }


        [Test]
        public void CastStringToDouble1()
        {
            var i = "1,2";
            double d = TypeCaster.Cast<double>(i);
            Assert.AreEqual(1.2, d);
        }

        [Test]
        public void CastStringToDouble2()
        {
            var i = "1.2";
            double d = TypeCaster.Cast<double>(i);
            Assert.AreEqual(1.2, d);
        }
    }
}