﻿using System.Collections;

namespace CSharpExtension.BitArrayExtensions
{
    public static class BitArrayExtension
    {
        public static byte[] ToBytes(this BitArray bits)
        {
            var byteCount = bits.Count/8;
            if (bits.Count%8 != 0) byteCount++;
            var bytes = new byte[byteCount];
            var byteIndex = 0;
            var bitIndex = 0;
            for (var i = 0; i < bits.Count; i++)
            {
                if (bits[i])
                    bytes[byteIndex] |= (byte) (1 << (7 - bitIndex));
                bitIndex++;
                if (bitIndex != 0) continue;
                bitIndex = 0;
                byteIndex++;
            }
            return bytes;
        }
    }
}
