﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSharpExtension.ObjectExtension;
using CSharpExtension.StringExtension;

namespace CSharpExtension
{
    public static class Contract
    {
        /// <summary>
        /// Проверяет условие
        /// </summary>
        /// <typeparam name="T">Тип исключения выбрасываемый при неудовлетворении условия</typeparam>
        /// <param name="сondition">Условие</param>
        /// <param name="message">Сообщение которое содержит исключение</param>
        /// <exception cref="System.Exception">Бросает исключение типа T</exception>
        public static void Requires<T>(bool сondition, string message = null) where T : Exception
        {
            if (сondition) return;
            if (message.IsNullOrEmpty())
                throw (T)Activator.CreateInstance(typeof(T));
            throw (T)Activator.CreateInstance(typeof(T), message);
        }

        /// <summary>
        /// Проверяет, что объект не является null 
        /// </summary>
        /// <param name="obj">объект</param>
        /// <param name="message">Сообщение которое содержит исключение</param>
        /// <exception cref="NullReferenceException">Бросает исключение типа NullReferenceException</exception>
        public static void RequiresNotNull(object obj, string message = null)
        {
            //if (obj != null) return;
            //var exception = message.IsNullOrEmpty() ? new NullReferenceException() : new NullReferenceException(message);
            //throw exception;
            Requires<NullReferenceException>(obj.IsNotNull(), message);
        }

        /// <summary>
        /// Проверяет, что аргумент не является null 
        /// </summary>
        /// <param name="obj">объект</param>
        /// <param name="message">Сообщение которое содержит исключение</param>
        /// <exception cref="ArgumentNullException">Бросает исключение типа ArgumentNullException</exception>
        public static void RequiresArgumentNotNull(object obj, string message = null)
        {
            //if (obj != null) return;
            //var exception = message.IsNullOrEmpty() ? new ArgumentException() : new ArgumentException(message);
            //throw exception;
            Requires<ArgumentNullException>(obj.IsNotNull(), message);
        }
    }
}
