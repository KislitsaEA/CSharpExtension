﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace CSharpExtension.Convertors
{
    public class BaseValueConvertor : IValueConverter
    {
        public double D { get; }

        public BaseValueConvertor() : this(1) { }

        public BaseValueConvertor(double d)
        {
            D = d;
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var val = ObsTypeCaster.Cast(value, default(double));
            return val/D;
            //throw new NotImplementedException();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var val = ObsTypeCaster.Cast(value, default(double));
            return val * D;
        }
    }
}
