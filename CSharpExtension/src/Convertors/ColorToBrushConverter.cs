﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace CSharpExtension.Convertors
{
    public class ColorToBrushConverter: IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value is Color color)
                return new SolidColorBrush(color);
            throw new ArgumentOutOfRangeException($"Параметр {nameof(value)} не является типа {typeof(Color)}. Тип параметра {value?.GetType().FullName ?? "null"}.");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}