using System;
using System.Windows;
using System.Windows.Data;

namespace CSharpExtension.Convertors
{
    public class CombiningConverter : IValueConverter
    {
        public IValueConverter Converter1 { get; set; }
        public IValueConverter Converter2 { get; set; }

        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var convertedValue = Converter1.Convert(value, targetType, parameter, culture);
            return Converter2.Convert(convertedValue, targetType, parameter, culture);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var convertedValue = Converter2.ConvertBack(value, targetType, parameter, culture);
            return Converter1.ConvertBack(convertedValue, targetType, parameter, culture);
        }

        #endregion
    }
}