using System;
using System.Globalization;
using System.Windows.Data;

namespace CSharpExtension.Convertors
{
    public class EnumDescriptionConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                var myEnum = (Enum)value;
                return EnumHelper.GetDescription(myEnum);
            }
            catch (Exception ex)
            {
                return "<<�� ����������>>";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }
}