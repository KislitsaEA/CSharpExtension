using System;
using System.Collections;
using System.Globalization;
using System.Windows.Data;
using CSharpExtension.IEnumerableExtensions;

namespace CSharpExtension.Convertors
{
    public class EnumerableVisibilityConverter : IValueConverter
    {
        private readonly IValueConverter _converter = new BoolToVisibilityOrCollapsedConverter();

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var enumerable = value as IEnumerable;
            return _converter.Convert(enumerable != null && enumerable.Any(), targetType, parameter, culture);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}