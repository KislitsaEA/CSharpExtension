﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Data;
using CSharpExtension.IEnumerableExtensions;

namespace CSharpExtension.Convertors
{
    public class EqialToBoolMultiConverter : IMultiValueConverter
    {
        //public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        //{
        //    return value.Equals(parameter);
        //}

        //public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        //{
        //    throw new NotImplementedException();
        //}
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if(values==null || values.Empty()) return false;
            var first = values.First();
            if (first == null) return false;
            return values.All(a => first.Equals(a));
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}