using System;
using System.Windows.Data;

namespace CSharpExtension.Convertors
{
    public class InverseBooleanConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            if (targetType == typeof (bool))
                return !(bool) value;
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            //throw new NotSupportedException();
            return Convert(value, targetType, parameter, culture);
        }

        #endregion
    }
}