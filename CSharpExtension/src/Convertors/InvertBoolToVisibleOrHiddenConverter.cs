using System;
using System.Globalization;
using System.Windows.Data;

namespace CSharpExtension.Convertors
{
    public class InvertBoolToVisibleOrHiddenConverter:IValueConverter
    {
        private readonly IValueConverter _boolToVisibilityConverter= new BoolToVisibleOrHiddenConverter();
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var val = (bool)value;
            return _boolToVisibilityConverter.Convert(!val, targetType, parameter, culture);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var res = _boolToVisibilityConverter.ConvertBack(value, targetType, parameter, culture);
            var val = (bool) res;
            return !val;
        }
    }
}