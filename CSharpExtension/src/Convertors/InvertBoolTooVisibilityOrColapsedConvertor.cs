using System;
using System.Globalization;
using System.Windows.Data;

namespace CSharpExtension.Convertors
{
    public class InvertBoolTooVisibilityOrColapsedConvertor : IValueConverter
    {
        private readonly IValueConverter _boolToVisibilityOrCollapsedConverter = new BoolToVisibilityOrCollapsedConverter();
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var val = (bool)value;
            return _boolToVisibilityOrCollapsedConverter.Convert(!val, targetType, parameter, culture);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var res = _boolToVisibilityOrCollapsedConverter.ConvertBack(value, targetType, parameter, culture);
            var val = (bool)res;
            return !val;
        }
    }
}