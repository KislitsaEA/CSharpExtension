﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace CSharpExtension.Convertors
{
    public class KilogramsToTonnes : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return null;
            var tmp = ObsTypeCaster.Cast<double>(value);
            return tmp / 1000.0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return null;
            var tmp = ObsTypeCaster.Cast<double>(value);
            return tmp * 1000.0;
        }
    }
}