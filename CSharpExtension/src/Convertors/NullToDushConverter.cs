using System;
using System.Globalization;
using System.Windows.Data;

namespace CSharpExtension.Convertors
{
    public class NullToDushConverter : IValueConverter
    {
        private string dush = "-";
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value ?? dush;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (string) value == dush ? null : ObsTypeCaster.Cast(value, GetDefault(targetType));
        }

        private static object GetDefault(Type type)
        {
            if (type.IsValueType)
            {
                return Activator.CreateInstance(type);
            }
            return null;
        }
    }
}