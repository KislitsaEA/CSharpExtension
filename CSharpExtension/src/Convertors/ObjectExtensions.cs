﻿namespace CSharpExtension.Convertors
{
    static class ObjectExtensions
    {
        public static double? ToDouble(this object parameter)
        {
            if (!double.TryParse(parameter?.ToString(), out var param)) return null;
            return param;
        }
    }
}