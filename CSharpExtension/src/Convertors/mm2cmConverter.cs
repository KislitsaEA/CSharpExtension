﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace CSharpExtension.Convertors
{
    /// <summary>
    /// конвертирование мм <-> см
    /// </summary>
    public class Mm2CmConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var tmp = ObsTypeCaster.Cast<double>(value,0);
            return tmp/10.0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var tmp = ObsTypeCaster.Cast<double>(value,0);
            return tmp *10.0;
        }
    }
}
