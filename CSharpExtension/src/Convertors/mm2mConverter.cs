﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace CSharpExtension.Convertors
{
    public class Mm2MConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var tmp = ObsTypeCaster.Cast<double>(value,0);
            return tmp / 1000.0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var tmp = ObsTypeCaster.Cast<double>(value,0);
            return tmp * 1000.0;
        }
    }
}
