﻿namespace CSharpExtension.DateTimes
{
    public static class DateTimeExtension
    {
        public static System.DateTime TrimSeconds(this System.DateTime time)
        {
            return new System.DateTime(time.Year, time.Month, time.Day, time.Hour, time.Minute, 0);
        }

        public static System.DateTime TrimMinutes(this System.DateTime time)
        {
            return new System.DateTime(time.Year, time.Month, time.Day, time.Hour, 0, 0);
        }

        public static System.DateTime TrimHours(this System.DateTime time)
        {
            return new System.DateTime(time.Year, time.Month, time.Day, 0, 0, 0);
        }
    }
}
