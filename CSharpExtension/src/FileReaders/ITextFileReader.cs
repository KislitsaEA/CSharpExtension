﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace CSharpExtension.FileReaders
{
    public interface ITextFileReader
    {
        IEnumerable<string> ReadLines();
        Task<IEnumerable<string>> ReadLinesAsync();
        string ReadToEnd();
        Task<string> ReadToEndAsync();

        string FilePath { get; }
    }
}