﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace CSharpExtension.FileReaders
{
    public class TextFileReader : ITextFileReader
    {
        private readonly string _path;
        private readonly Encoding _encoding;

        public TextFileReader(string path, Encoding encoding = null)
        {
            Contract.Requires<ArgumentNullException>(!string.IsNullOrEmpty(path), nameof(path));
            if(!File.Exists(path))
                throw new FileNotFoundException(path);
            _path = path;
            _encoding = encoding ?? Encoding.Default;
        }

        public string ReadToEnd()
        {
            using (var sr = new StreamReader(_path,_encoding))
            {
                var line = sr.ReadToEnd();
                return line;
            }
        }

        public async Task<string> ReadToEndAsync()
        {
            using (var sr = new StreamReader(_path, _encoding))
            {
                var line = await sr.ReadToEndAsync();
                return line;
            }
        }

        public string FilePath => _path;

        public IEnumerable<string> ReadLines()
        {
            using (var sr = new StreamReader(_path, _encoding))
            {
                while (!sr.EndOfStream)
                {
                    yield return sr.ReadLine();
                }
            }
        }

        public Task<IEnumerable<string>> ReadLinesAsync()
        {
            return Task.Run(() => ReadLines());
        }
    }
}
