﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace CSharpExtension.Fronts
{
    /// <summary>
    /// Автоматические проверяет возникновение фронта
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class AutoFrontCacher<T> : IFrontable
    {
        private readonly T _setting;
        private readonly Func<T, T, bool> _predicate;
        private readonly FrontEventArgs _directFrontEventArgs;
        private readonly FrontEventArgs _backFrontEventArgs;
        private readonly Func<T> _func;
        private volatile Timer _timer;
        private readonly int _notifyDelayTime;

        public AutoFrontCacher(T setting, Func<T, T, bool> predicate, Func<T> func, int silenceTime, int delayTime = 0) : this(setting, predicate, null as FrontEventArgs, null as FrontEventArgs, func, delayTime, silenceTime) { }
        public AutoFrontCacher(T setting, Func<T, T, bool> predicate, FrontEventArgs directFrontEventArgs, FrontEventArgs backFrontEventArgs, Func<T> func, int notifyDelayTime = 0, int silenceTime=0)
        {
            _setting = setting;
            _predicate = predicate;
            _func = func;
            _notifyDelayTime = notifyDelayTime;
            _directFrontEventArgs = directFrontEventArgs ?? new FrontEventArgs() { Message = "Прямой фронт" };
            _backFrontEventArgs = backFrontEventArgs ?? new FrontEventArgs() { Message = "Обратный фронт" };
            _timer = new Timer( Check);
            _timer.Change(TimeSpan.FromSeconds(silenceTime), TimeSpan.FromSeconds(1));
        }

        public AutoFrontCacher(T setting, Func<T, T, bool> predicate, string directFrontEventArgsMessage,
            string backFrontEventArgsMessage, Func<T> func, int delayTime = 0, int silenceTime=0) : this(setting, predicate, new FrontEventArgs() { Message = directFrontEventArgsMessage }, new FrontEventArgs() { Message = backFrontEventArgsMessage }, func, delayTime, silenceTime)
        {

        }

        public bool CheckValue(T value)
        {
            var newFront = _predicate(_setting, value);
            UpdateFront(newFront);
            return Front;
        }

        public bool Front { get; private set; }
        public event EventHandler<FrontEventArgs> DirectFront;
        public event EventHandler<FrontEventArgs> BackFront;

        //==
        private void UpdateFront(bool newFront)
        {
            if (Front == newFront) return;
            Front = newFront;
            if (Front)
            {
                Task.Run(() =>
                {
                    Thread.Sleep(GetDelayTime(_notifyDelayTime));
                    DirectFront?.Invoke(this, _directFrontEventArgs);
                });
            }
            else
            {
                Task.Run(() =>
                {
                    Thread.Sleep(GetDelayTime(100 - _notifyDelayTime));
                    BackFront?.Invoke(this, _backFrontEventArgs);
                });
            }
        }

        private int GetDelayTime(int time) => time > 0 ? time : 0;


        private void Check(object o)
        {
            _timer?.Change(Timeout.Infinite, Timeout.Infinite);
            try
            {
                if (_func == null) return;
                var value = _func();
                CheckValue(value);
            }
            catch (Exception)
            {
                //ignore
            }
            finally
            {
                _timer?.Change(TimeSpan.FromSeconds(1), TimeSpan.Zero);
            }
        }

        public void Dispose()
        {
            var timer = _timer;
            _timer = null;
            timer?.Change(Timeout.Infinite, Timeout.Infinite);
            timer?.Dispose();
        }
    }
}