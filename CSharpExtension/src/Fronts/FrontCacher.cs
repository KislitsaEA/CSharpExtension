﻿using System;

namespace CSharpExtension.Fronts
{
    /// <summary>
    /// Проверяет возникновение фронта при вызове метода проверки
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class FrontCacher<T> : IFrontable
    {
        private readonly T _setting;
        private readonly Func<T, T, bool> _predicate;
        private readonly FrontEventArgs _directFrontEventArgs;
        private readonly FrontEventArgs _backFrontEventArgs;

        public FrontCacher(T setting, Func<T, T, bool> predicate) : this(setting, predicate, null as FrontEventArgs, null as FrontEventArgs) { }
        public FrontCacher(T setting, Func<T, T, bool> predicate, FrontEventArgs directFrontEventArgs, FrontEventArgs backFrontEventArgs)
        {
            _setting = setting;
            _predicate = predicate;
            _directFrontEventArgs = directFrontEventArgs ?? new FrontEventArgs() { Message = "Прямой фронт" };
            _backFrontEventArgs = backFrontEventArgs ?? new FrontEventArgs() { Message = "Обратный фронт" };
        }

        public FrontCacher(T setting, Func<T, T, bool> predicate, string directFrontEventArgsMessage,
            string backFrontEventArgsMessage) : this(setting, predicate, new FrontEventArgs() { Message = directFrontEventArgsMessage }, new FrontEventArgs() { Message = backFrontEventArgsMessage })
        {

        }

        public bool CheckValue(T value)
        {
            var newFront = _predicate(_setting, value);
            UpdateFront(newFront);
            return Front;
        }

        public bool Front { get; private set; }
        public event EventHandler<FrontEventArgs> DirectFront;
        public event EventHandler<FrontEventArgs> BackFront;

        //==
        private void UpdateFront(bool newFront)
        {
            if (Front == newFront) return;
            Front = newFront;
            if (Front)
                DirectFront?.Invoke(this, _directFrontEventArgs);
            else
            {
                BackFront?.Invoke(this, _backFrontEventArgs);
            }
        }

        public virtual void Dispose()
        {
            //throw new NotImplementedException();
        }
    }
}
