﻿using System;

namespace CSharpExtension.Fronts
{
    public class FrontEventArgs : EventArgs
    {
        public string Message { get; set; }

    }
}