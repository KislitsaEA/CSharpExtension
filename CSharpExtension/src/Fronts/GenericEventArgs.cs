﻿using System;

namespace CSharpExtension.Fronts
{
    public class GenericEventArgs<T> : EventArgs
    {
        public T Value { get; set; }
    }
}