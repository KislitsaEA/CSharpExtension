﻿using System;

namespace CSharpExtension.Fronts
{
    /// <summary>
    /// Базовый интерфейс отслеживания фронта
    /// </summary>
    public interface IFrontable: IDisposable
    {
        /// <summary>
        /// Флаг, показывающий взведён ли фронт
        /// </summary>
        bool Front { get;}

        /// <summary>
        /// Событие при возникновении прямого фронта
        /// </summary>
        event EventHandler<FrontEventArgs> DirectFront;

        /// <summary>
        /// Событие при возникновении обратного
        /// </summary>
        event EventHandler<FrontEventArgs> BackFront;
    }
}