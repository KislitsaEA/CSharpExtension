﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpExtension.ICollectionExtensions
{
    public static class CollectionExtension
    {
        public static void AddMany<T>(this ICollection<T> collection, params T[] items)
        {
            collection.AddRange(items);
        }

        public static void AddRange<T>(this ICollection<T> collection, IEnumerable<T> items)
        {
            foreach (var item in items)
            {
                collection.Add(item);
            }
        }
    }
}
