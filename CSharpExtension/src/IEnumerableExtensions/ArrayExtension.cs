﻿using System;
using System.Collections.Generic;

namespace CSharpExtension.IEnumerableExtensions
{
    public static class ArrayExtension
    {
        public static IEnumerable<T> Cast<T>(this Array collection)
        {
            var list = new List<T>();
            foreach (var item in collection)
            {
                try
                {
                    var t = (T) item;
                    list.Add(t);
                }
                catch (Exception e) 
                {
                    // ignored
                }
            }
            return list;
        }
    }
}