﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Controls;

namespace CSharpExtension.IEnumerableExtensions
{
    public static class EnumerableExtension
    {
        public static IEnumerable<IEnumerable<T>> Sliding<T>(this IEnumerable<T> collection, int size)
        {
            while (collection.Any())
            {
                yield return collection.Take(size);
                collection = collection.Skip(size);
            }
        }

        public static void ForEach<T>(this IEnumerable<T> collection, Action<T> action)
        {
            foreach (var item in collection)
            {
                action(item);
            }
        }

        public static IReadOnlyCollection<T> ToReadOnlyCollection<T>(this IEnumerable<T> collection)
        {
            return new ReadOnlyCollection<T>(collection.ToList());
        }


        public static bool Empty<T>(this IEnumerable<T> collection)
        {
            return !collection.Any();
        }

        public static bool NotAny<T>(this IEnumerable<T> collection, Func<T, bool> predicate)
        {
            return !collection.Any(predicate);
        }

        //public static Nullable<T> Min1<T>(this IEnumerable<T> collection, Func<T, bool> predicate)
        //{
        //    return null;
        //}

        /// <summary>
        /// Среднее арифметическое взвешенное
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection"></param>
        /// <param name="value"></param>
        /// <param name="weight"></param>
        /// <returns></returns>
        public static double? WeightedArithmeticMean<T>(this IEnumerable<T> collection, Func<T,double?> value, Func<T, double?> weight)
        {
            var totalWeight = collection.Sum(weight);
            if (!totalWeight.HasValue || totalWeight == 0)
                return collection.Average(value);
            var totalValueWeight = collection.Select(s => value(s) * weight(s)).Sum();
            return totalValueWeight / totalWeight;
        }

        public static double? SumNull<T>(this IEnumerable<T> collection, Func<T, double?> value)
        {
            if (collection.All(a => !value(a).HasValue))
                return null;
            return collection.Sum(value);
        }

        //==========================================
        //public static T AverageIfAny<T>(this IEnumerable<T> collection, Func<T, T, T> predicate, T defaultVlue)
        //{
        //    if (collection.Empty())
        //        return defaultVlue;
        //    return collection.Aggregate(predicate);
        //}

        //public static T AverageIfAny<T>(this IEnumerable<T> collection, Func<T, Numeric> predicate, T defaultVlue)
        //{
        //    if (collection.Empty())
        //        return defaultVlue;
        //    return collection.Aggregate(predicate);
        //}

        //public static T SumIfAny<T>(this IEnumerable<T> collection, Func<T, T, T> predicate, T defaultVlue)
        //{
        //    if (collection.Empty())
        //        return defaultVlue;
        //    return collection.Sum(predicate);
        //}

        //==========================================

        public static bool Any(this IEnumerable collection)
        {
            var count = 0;
            foreach (var o in collection)
            {
                count++;
                if (count > 0) return true;
            }
            return count > 0;
        }

        /// <summary>
        /// Перемешивает элементы последовательности
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="self"></param>B
        /// <returns></returns>
        public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> self)
        {
            return self.Select(s => new
            {
                Item = s,
                weight = ShuffleRnd.Next(0, 1000000)
            }).OrderBy(o => o.weight).Select(s => s.Item).ToArray();
        }
        private static readonly Random ShuffleRnd = new Random(DateTime.Now.Millisecond);

        private class PredicateEqualityComparer<TSource, TKey> : IEqualityComparer<TSource>
        {
            private readonly Func<TSource, TKey> _keySelector;

            public PredicateEqualityComparer(Func<TSource, TKey> keySelector) => _keySelector = keySelector;

            public bool Equals(TSource x, TSource y) => _keySelector(x).Equals(_keySelector(y));

            public int GetHashCode(TSource obj) => _keySelector(obj).GetHashCode();
        }

        public static IEnumerable<TSource> Distinct<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector) =>
            source.Distinct(new PredicateEqualityComparer<TSource, TKey>(keySelector));
    }
}
