﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpExtension.ListExtensions
{
    public static class ListExtension
    {
        public static void AddMany<T>(this List<T> list, params T[] items)
        {
            list.AddRange(items);
        }
    }
}
