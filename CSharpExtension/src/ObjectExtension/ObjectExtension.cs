﻿namespace CSharpExtension.ObjectExtension
{
    public static class ObjectExtension
    {
        /// <summary>
        /// Проверяет равен ли объект null
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static bool IsNull(this object o)
        {
            return o == null;
        }

        /// <summary>
        /// Проверят, что объект не равен null
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static bool IsNotNull(this object o) => !o.IsNull();
    }
}