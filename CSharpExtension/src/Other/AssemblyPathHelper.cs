using System;
using System.IO;
using System.Reflection;

namespace CSharpExtension
{
    public static class AssemblyPathHelper
    {
        public static string GetCurrentAssemblyPath
        {
            get
            {
                var codeBase = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase);
                var uri = new UriBuilder(codeBase);
                var path = Uri.UnescapeDataString(uri.Path);
                return path;
            }
        }
    }
}