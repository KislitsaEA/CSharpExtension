﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSharpExtension.FileReaders;
using CSharpExtension.StringExtension;

namespace CSharpExtension
{
    public class CsvReader
    {
        private readonly ITextFileReader _reader;

        public CsvReader(ITextFileReader reader, string delimeter, string comment)
        {
            _reader = reader;
            Delimeter = delimeter;
            Comment = comment;
        }

        public CsvReader(ITextFileReader reader): this(reader, ";", "#"){}

        public CsvReader(string filePath) : this(filePath, ";", "#") { }
        public CsvReader(string filePath, string delimeter, string comment): this(new TextFileReader(filePath), delimeter, comment){}
        
        public string Delimeter { get; }
        public string Comment { get; }

        public string[][] Read()
        {
            var data = _reader.ReadToEnd();
            var rows = data.SplitByLines();
            rows = rows.Where(w => !IsRowEmpty(w) && !IsRowComment(w)).ToArray();
            return rows.Select(s => s.Split(new[] { Delimeter }, StringSplitOptions.None)).ToArray();
        }

        private bool IsRowEmpty(string row)
        {
            var pureString = row.Replace(Delimeter, string.Empty);
            return string.IsNullOrEmpty(pureString) || string.IsNullOrWhiteSpace(pureString);
        }

        private bool IsRowComment(string row)
        {
            return row.TrimStart().StartsWith(Comment);
        }
    }
}
