﻿using System;

namespace CSharpExtension
{
    /// <summary>
    /// Расширения для типа данных Double
    /// </summary>
    public static class DoubleExtension
    {
        /// <summary>
        /// Возвращает значение defaultValue если значение Nan
        /// </summary>
        /// <param name="value">Значение переменной</param>
        /// <param name="defaultValue">Значение по умолчанию</param>
        /// <returns></returns>
        public static double ValueIfNan(this double value, double defaultValue = 0.0)
        {
            return double.IsNaN(value) ? defaultValue : value;
        }

        /// <summary>
        /// Возвращает значение defaultValue если значение Infinity
        /// </summary>
        /// <param name="value">Значение переменной</param>
        /// <param name="defaultValue">Значение по умолчанию</param>
        /// <returns></returns>
        public static double ValueIfInfinity(this double value, double defaultValue = 0.0)
        {
            return double.IsInfinity(value) ? defaultValue : value;
        }

        /// <summary>
        /// Возвращает значение defaultValue если значение Nan или Infinity
        /// </summary>
        /// <param name="value">Значение переменной</param>
        /// <param name="defaultValue">Значение по умолчанию</param>
        /// <returns></returns>
        public static double ValueIfNanOrInfinity(this double value, double defaultValue = 0.0)
        {
            return double.IsInfinity(value) || double.IsNaN(value) ? defaultValue : value;
        }


        public static bool IsNanOrInfinity(this double value)
        {
            //return double.IsInfinity(value) || double.IsNaN(value) ? defaultValue : value;

            return double.IsNaN(value) || double.IsInfinity(value);
        }

        public static bool NotNanAndNotInfinity(this double value)
        {
            //return double.IsInfinity(value) || double.IsNaN(value) ? defaultValue : value;

            return !double.IsNaN(value) && !double.IsInfinity(value);
        }

        public static double Round(this double value, int nums)
        {
            //return double.IsInfinity(value) || double.IsNaN(value) ? defaultValue : value;

            return Math.Round(value, nums, MidpointRounding.AwayFromZero);
        }
    }
}