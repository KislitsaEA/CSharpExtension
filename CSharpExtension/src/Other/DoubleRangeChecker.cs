﻿using System.Linq;

namespace CSharpExtension
{
    public class DoubleRangeChecker : IRangeChecker<double>
    {
        public double MinRange { get; }
        public double MaxRange { get; }

        public DoubleRangeChecker(double minRange, double maxRange)
        {
            var arr = new[] {minRange, maxRange};
            MinRange = arr.Min();
            MaxRange = arr.Max();
        }

        public bool IsInRange(double value) =>  MinRange <= value && value <= MaxRange;

        public double CheckValue(double value)
        {
            if (value <= MinRange) return MinRange;
            if (MaxRange<= value) return MaxRange;
            return value;
        }
    }
}
