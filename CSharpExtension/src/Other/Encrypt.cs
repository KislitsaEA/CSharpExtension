﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace CSharpExtension
{
    public static class Encrypt
    {
        // This size of the IV (in bytes) must = (keysize / 8).  Default keysize is 256, so the IV must be
        // 32 bytes long.  Using a 16 character string here gives us 32 bytes when converted to a byte array.
        private const string InitVector = "pemgail9uzpgzl88";
        // This constant is used to determine the keysize of the encryption algorithm
        private const int Keysize = 256;
        //Encrypt
        private const string SaltStart = "«««";
        private const string SaltEnd = "»»»";

        private static string AddSalt(string str)
        {
            return SaltStart + str + SaltEnd;
        }

        private static string RemoveSalt(string str)
        {
            return str.TrimStart(SaltStart.ToCharArray()).TrimEnd(SaltEnd.ToCharArray());
        }

        private static bool HaseSalt(string str)
        {
            return str.StartsWith(SaltStart) && str.EndsWith(SaltEnd);
        }

        //public static string DefaultPassPhrase => string.Empty;
        public static string DefaultPassPhrase => "7193be64-2178-47ea-ae9a-69b509b185f3";

        public static string EncryptString(string plainText) => EncryptString(plainText, DefaultPassPhrase);

        public static string EncryptString(string plainText, string passPhrase)
        {
            plainText = AddSalt(plainText);
            byte[] initVectorBytes = Encoding.UTF8.GetBytes(InitVector);
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, null);
            byte[] keyBytes = password.GetBytes(Keysize/8);
            RijndaelManaged symmetricKey = new RijndaelManaged();
            symmetricKey.Mode = CipherMode.CBC;
            ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
            cryptoStream.FlushFinalBlock();
            byte[] cipherTextBytes = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();
            return Convert.ToBase64String(cipherTextBytes);
        }

        public static bool IsEncrypted(string text, string passPhrase = null)
        {
            string str;
            try
            {
                str = DecryptStringPrivate(text, string.IsNullOrEmpty(passPhrase) ? DefaultPassPhrase : passPhrase);
            }
            catch (Exception)
            {
                return false;
            }
            return HaseSalt(str);
        }


        public static string DecryptString(string cipherText) => DecryptString(cipherText, DefaultPassPhrase);

        public static string DecryptString(string cipherText, string passPhrase)
        {
            var str = DecryptStringPrivate(cipherText, passPhrase);
            return RemoveSalt(str);
        }
        //Decrypt
        private static string DecryptStringPrivate(string cipherText) => DecryptStringPrivate(cipherText, DefaultPassPhrase);
        private static string DecryptStringPrivate(string cipherText, string passPhrase)
        {
            byte[] initVectorBytes = Encoding.ASCII.GetBytes(InitVector);
            byte[] cipherTextBytes = Convert.FromBase64String(cipherText);
            PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, null);
            byte[] keyBytes = password.GetBytes(Keysize / 8);
            RijndaelManaged symmetricKey = new RijndaelManaged();
            symmetricKey.Mode = CipherMode.CBC;
            ICryptoTransform decryptor = symmetricKey.CreateDecryptor(keyBytes, initVectorBytes);
            MemoryStream memoryStream = new MemoryStream(cipherTextBytes);
            CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
            byte[] plainTextBytes = new byte[cipherTextBytes.Length];
            int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
            memoryStream.Close();
            cryptoStream.Close();
            return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
        }
    }
}