using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using CSharpExtension.IEnumerableExtensions;

namespace CSharpExtension
{
    public static class EnumHelper
    {
        public static string GetDescription(Enum en)
        {
            if (en == null) return string.Empty;
            var type = en.GetType();
            var memInfo = type.GetMember(en.ToString());
            if (memInfo.Length <= 0) return en.ToString();
            var attrs = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attrs.Length > 0 ? ((DescriptionAttribute)attrs[0]).Description : en.ToString();
        }

        public static IEnumerable<T> GetEnumValues<T>()
        {
            if (typeof(T).IsEnum)
                return Enum.GetValues(typeof(T)).Cast<T>();
            throw new InvalidOperationException($"{typeof(T)} is not Enum type.");
        }

        public static TEnum? ParseEnum<TEnum>(int enumValue) where TEnum : struct
        {
            var type = typeof(TEnum);
            if (!type.IsEnum)
                throw new InvalidCastException($"{type} is not Enum");
            if (!Enum.IsDefined(typeof(TEnum), enumValue))
                return null;
            return (TEnum)Enum.ToObject(typeof(TEnum), enumValue);
        }
    }
}