﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpExtension
{
    public static class GenericExtension
    {
        /// <summary>
        /// Определяет входили элемент в последовательность
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj">Элемент</param>
        /// <param name="args">Последовательность</param>
        /// <returns></returns>
        public static bool IsIn<T>(this T obj, params T[] args) => args.Contains(obj);

        //здесь надо специфицировать типы как сравниваемые
        public static bool IsInRange<T>(this T obj, T min, T max)
        {
            return Comparer<T>.Default.Compare(min, obj) < 0 && Comparer<T>.Default.Compare(obj, max) < 0;
        }

        public static bool IsInRangeEqualMin<T>(this T obj, T min, T max)
        {
            return Comparer<T>.Default.Compare(min, obj) <= 0 && Comparer<T>.Default.Compare(obj, max) < 0;
        }

        public static bool IsInRangeEqualMax<T>(this T obj, T min, T max)
        {
            return Comparer<T>.Default.Compare(min, obj) < 0 && Comparer<T>.Default.Compare(obj, max) <= 0;
        }

        public static bool IsInEqualRange<T>(this T obj, T min, T max)
        {
            return Comparer<T>.Default.Compare(min, obj) <= 0 && Comparer<T>.Default.Compare(obj, max) <= 0;
        }


        public static T Min<T>(params T[] list)
        {
            return list.Min();
        }

        public static T Max<T>(params T[] list)
        {
            return list.Max();
        }
    }
}
