﻿namespace CSharpExtension
{
    /// <summary>
    /// Выполняет проверку значений на вхождение в диапазон допустимых
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IRangeChecker<T>
    {
        /// <summary>
        /// Нижняя граница допустимого диапазона
        /// </summary>
        T MinRange { get; }

        /// <summary>
        /// Верхняя граница допустимого диапазона
        /// </summary>
        T MaxRange { get; }
        /// <summary>
        /// Проверяет, входит ли значение в диапазон допустимых
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        bool IsInRange(T value);

        /// <summary>
        /// Возвращает значение или ближайшую границу диапазона допустимых значений если значение в него не входит
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        T CheckValue(T value);
    }
}