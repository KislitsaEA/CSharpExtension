﻿using System.Linq;

namespace CSharpExtension
{
    public class IntegerRangeChecker : IRangeChecker<int>
    {
        public int MinRange { get; }
        public int MaxRange { get; }

        public IntegerRangeChecker(int minRange, int maxRange)
        {
            var arr = new[] { minRange, maxRange };
            MinRange = arr.Min();
            MaxRange = arr.Max();
        }

        public bool IsInRange(int value) => MinRange <= value && value <= MaxRange;

        public int CheckValue(int value)
        {
            if (value <= MinRange) return MinRange;
            if (MaxRange <= value) return MaxRange;
            return value;
        }
    }
}