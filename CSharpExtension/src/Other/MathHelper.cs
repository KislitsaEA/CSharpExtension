﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpExtension
{
    public class MathHelper
    {
        /// <summary>
        /// Возвращает значение или значение по умолчанию если значение меньше уставки
        /// </summary>
        /// <param name="value">Значение</param>
        /// <param name="limit">Уставка</param>
        /// <param name="defaultValue">Значение по умолчанию</param>
        /// <returns></returns>
        public static double ValueOrDefaultIfLess(double value, double limit, double defaultValue = 0)
        {
            return value < limit ? defaultValue : value;
        }

        /// <summary>
        /// Возвращает значение или значение по умолчанию если значение меньше или равно уставке
        /// </summary>
        /// <param name="value">Значение</param>
        /// <param name="limit">Уставка</param>
        /// <param name="defaultValue">Значение по умолчанию</param>
        /// <returns></returns>
        public static double ValueOrDefaultIfLessOrEqual(double value, double limit, double defaultValue = 0)
        {
            return value <= limit ? defaultValue : value;
        }

        /// <summary>
        /// Возвращает значение или значение по умолчанию если значение больше уставки
        /// </summary>
        /// <param name="value">Значение</param>
        /// <param name="limit">Уставка</param>
        /// <param name="defaultValue">Значение по умолчанию</param>
        /// <returns></returns>
        public static double ValueOrDefaultIfMore(double value, double limit, double defaultValue = 0)
        {
            return value > limit ? defaultValue : value;
        }

        /// <summary>
        /// Возвращает значение или значение по умолчанию если значение больше или равно уставке
        /// </summary>
        /// <param name="value">Значение</param>
        /// <param name="limit">Уставка</param>
        /// <param name="defaultValue">Значение по умолчанию</param>
        /// <returns></returns>
        public static double ValueOrDefaultIfMoreOrEqual(double value, double limit, double defaultValue = 0)
        {
            return value >= limit ? defaultValue : value;
        }
    }
}
