﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CSharpExtension
{
    public static class MessageBoxHelper
    {
        public static MessageBoxResult ShowErrorMessageBox(Exception ex)
        {
            return MessageBox.Show(ex.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
        }


        public static MessageBoxResult ShowErrorMessageBox(string message)
        {
            return MessageBox.Show(message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
        }
    }
}
