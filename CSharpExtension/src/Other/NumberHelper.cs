﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpExtension
{
    public class NumberHelper
    {
        public static bool IsIntegerNumber(object o)
        {
            return o is sbyte
                   || o is byte
                   || o is short
                   || o is ushort
                   || o is int
                   || o is uint
                   || o is long
                   || o is ulong
                   || o is Int16
                   || o is Int32
                   || o is Int64
                   || o is UInt16
                   || o is UInt32
                   || o is UInt64;
        }

        public static bool IsFloatNumber(object o)
        {
            return o is float
                   || o is double
                   || o is decimal
                   || o is Single;
        }

        public static bool IsNumber(object o)
        {
            return IsFloatNumber(o) || IsIntegerNumber(o);
        }
    }
}
