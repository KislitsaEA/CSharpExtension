using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace CSharpExtension
{
    public  class ObservableCollectionHelper
    {
        /// <summary>
        /// �������������� ObservableCollection<T> ������������� enumerable ���� ������������ ������ ��� null �������������� ������ ����������
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumerable"></param>
        /// <returns></returns>
        public static ObservableCollection<T> Initialize<T>(IEnumerable<T> enumerable)
        {
            if(enumerable!=null && enumerable.Any())
                return new ObservableCollection<T>(enumerable.ToArray());
            return new ObservableCollection<T>();
        }

        public static ObservableCollection<T> Initialize<T>() => Initialize<T>(null);
    }
}