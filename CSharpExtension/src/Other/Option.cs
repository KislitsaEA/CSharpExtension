﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpExtension
{
    public struct Option<T>
    {
        private readonly T _value;

        public T Value => _value;

        public bool HasValue => _value != null;

        public bool HasNoValue => !HasValue;

        private Option(T value)
        {
            _value = value;
        }

        public static implicit operator Option<T>(T value)
        {
            return new Option<T>(value);
        }
    }
}
