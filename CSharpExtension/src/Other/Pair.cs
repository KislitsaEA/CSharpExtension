﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpExtension
{
    public class Pair<T,TK>
    {
        public Pair(T item1, TK item2)
        {
            Item1 = item1;
            Item2 = item2;
        } 

        public T Item1 { get; set; }
        public TK Item2 { get; set; }
    }
}
