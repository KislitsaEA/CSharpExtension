﻿using System;
using System.Threading;

namespace CSharpExtension
{
    public static class TimeSpanExtension
    {
        public static string ToString(this TimeSpan timeSpan, string toStringIfInfinite, string fomat, IFormatProvider formatProvider = null)
        {
            return timeSpan == Timeout.InfiniteTimeSpan ? toStringIfInfinite : timeSpan.ToString(fomat, formatProvider);
        }


        public static bool IsInfiniteTimeSpan(this TimeSpan timeSpan)
        {
            return timeSpan == Timeout.InfiniteTimeSpan;
        }
    }
}