using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using CSharpExtension.StringExtension;
using Newtonsoft.Json;

namespace CSharpExtension
{







    [Obsolete("use XmlSerializer")]
    public class XmlSerializerHelper
    {
        private static System.Xml.Serialization.XmlSerializer GetXmlSerializer<T>(Type[] extraTypes = null)
        {
            return extraTypes == null
                ? new System.Xml.Serialization.XmlSerializer(typeof(T))
                : new System.Xml.Serialization.XmlSerializer(typeof(T), extraTypes);
        }

        public static string Serialize<T>(T value, Type[] extraTypes = null)
        {
            if (value == null) return null;
            var serializer = GetXmlSerializer<T>(extraTypes);
            var settings = new XmlWriterSettings
            {
                Encoding = new UnicodeEncoding(false, false),
                Indent = true,
                OmitXmlDeclaration = false,
            };
            // no BOM in a .NET string
            using (var textWriter = new StringWriter())
            {
                using (var xmlWriter = XmlWriter.Create(textWriter, settings))
                {
                    serializer.Serialize(xmlWriter, value);
                }
                return textWriter.ToString();
            }
        }

        public static void SerializeToFile<T>(T value, string filePath, Type[] extraTypes = null)
        {
            using (var sw = new StreamWriter(filePath))
            {
                var str = Serialize(value, extraTypes);
                sw.Write(str);
            }
        }

        public static T Deserialize<T>(string xml, Type[] extraTypes = null)
        {
            if (string.IsNullOrEmpty(xml)) return default(T);
            var serializer = GetXmlSerializer<T>(extraTypes);
            var settings = new XmlReaderSettings();
            // No settings need modifying here
            using (var textReader = new StringReader(xml))
            {
                using (var xmlReader = XmlReader.Create(textReader, settings))
                {
                    return (T)serializer.Deserialize(xmlReader);
                }
            }
        }

        public static T DeserializeFromFile<T>(string filePath, Type[] extraTypes = null)
        {
            using (var sr = new StreamReader(filePath))
            {
                var str = sr.ReadToEnd();
                return Deserialize<T>(str, extraTypes);
            }
        }
    }
}