﻿using System;
using System.IO;

namespace CSharpExtension.Serialization
{
    public abstract class AbsSerializer : ISerializer
    {
        public string Serialize<T>(T value) => Serialize(value, null);
        public abstract string Serialize<T>(T value, Type[] extraTypes);
        public void SerializeToFile<T>(FileInfo file, T value) => SerializeToFile(file, value, null);

        public void SerializeToFile<T>(FileInfo file, T value, Type[] extraTypes)
        {
            using (var sw = new StreamWriter(file.FullName))
            {
                var str = Serialize(value, extraTypes);
                sw.Write(str);
                sw.Close();
            }
        }

        public T Deserialize<T>(string str) => Deserialize<T>(str, null);
        public abstract T Deserialize<T>(string str, Type[] extraTypes);
        public T DeserializeFtomFile<T>(FileInfo file) => DeserializeFtomFile<T>(file, null);

        public T DeserializeFtomFile<T>(FileInfo file, Type[] extraTypes)
        {
            //using (var stringReader = new StringReader(file.FullName))
            using (var reader = new StreamReader(file.FullName))
            {
                var str = reader.ReadToEnd();
                return Deserialize<T>(str, extraTypes);
            }
        }
    }
}
