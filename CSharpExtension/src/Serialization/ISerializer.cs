﻿using System;
using System.IO;

namespace CSharpExtension.Serialization
{
    public interface ISerializer
    {
        string Serialize<T>(T value);
        string Serialize<T>(T value, Type[] extraTypes);
        void SerializeToFile<T>(FileInfo file, T value);
        void SerializeToFile<T>(FileInfo file, T value, Type[] extraTypes);


        T Deserialize<T>(string str);
        T Deserialize<T>(string str, Type[] extraTypes);
        T DeserializeFtomFile<T>(FileInfo file);
        T DeserializeFtomFile<T>(FileInfo file, Type[] extraTypes);
    }
}