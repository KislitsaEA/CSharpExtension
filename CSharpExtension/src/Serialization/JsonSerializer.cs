﻿using System;
using Newtonsoft.Json;

namespace CSharpExtension.Serialization
{
    public class JsonSerializer : AbsSerializer
    {
        public override string Serialize<T>(T value, Type[] extraTypes)
        {
            return JsonConvert.SerializeObject(value, Formatting.Indented);
        }

        public override T Deserialize<T>(string str, Type[] extraTypes)
        {
            return JsonConvert.DeserializeObject<T>(str);
        }
    }
}