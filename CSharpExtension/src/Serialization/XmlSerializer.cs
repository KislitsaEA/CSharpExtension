﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using CSharpExtension.StringExtension;

namespace CSharpExtension.Serialization
{
    public class XmlSerializer : AbsSerializer
    {
        protected static System.Xml.Serialization.XmlSerializer GetXmlSerializer<T>(Type[] extraTypes = null)
        {
            return extraTypes == null
                ? new System.Xml.Serialization.XmlSerializer(typeof(T))
                : new System.Xml.Serialization.XmlSerializer(typeof(T), extraTypes);
        }

        public override string Serialize<T>(T value, Type[] extraTypes)
        {
            Contract.RequiresNotNull(value, nameof(value));
            //
            var serializer = GetXmlSerializer<T>(extraTypes);
            var settings = new XmlWriterSettings
            {
                Encoding = new UnicodeEncoding(false, false),
                Indent = true,
                OmitXmlDeclaration = false,
            };
            // no BOM in a .NET string
            using (var textWriter = new StringWriter())
            {
                using (var xmlWriter = XmlWriter.Create(textWriter, settings))
                {
                    serializer.Serialize(xmlWriter, value);
                }
                return textWriter.ToString();
            }
        }

        public override T Deserialize<T>(string str, Type[] extraTypes)
        {
            Contract.Requires<ArgumentOutOfRangeException>(!str.IsNullOrEmpty(), $"Параметр {nameof(str)} не может быть null или пустой строкой.");

            var serializer = GetXmlSerializer<T>(extraTypes);
            var settings = new XmlReaderSettings();
            using (var textReader = new StringReader(str))
            {
                using (var xmlReader = XmlReader.Create(textReader, settings))
                {
                    return (T)serializer.Deserialize(xmlReader);
                }
            }
        }
    }
}