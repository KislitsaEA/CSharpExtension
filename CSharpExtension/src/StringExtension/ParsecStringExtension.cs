﻿using System;
using System.Globalization;
using Functional.Maybe;

namespace CSharpExtension.StringExtension
{
    public static class ParsecStringExtension
    {
        /// <summary>
        /// Выполняет парсинг исходной строки в Double
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static double? ToDouble(this string value)
        {
            double i;
            if (double.TryParse(value.Trim(), out i))
                return i;
            if (double.TryParse(value.Trim().Replace(',', '.'), NumberStyles.Any, CultureInfo.GetCultureInfo("en-US"), out i))
                return i;
            return null;
        }

        public static bool? ToBool(this string value)
        {
            bool i;
            if (bool.TryParse(value.Trim(), out i))
                return i;
            return null;
        }
        
        /// <summary>
        /// Выполняет парсинг исходной строки в Int32
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static int? ToInt(this string str)
        {
            int i;
            if (int.TryParse(str.Trim(), out i))
                return i;
            return null;
        }

        /// <summary>
        /// Выполняет парсинг строки в enum типа T
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="str"></param>
        /// <returns></returns>
        public static T? ToEnum<T>(this string str) where T: struct 
        {
            if(!typeof(T).IsEnum)
                throw new InvalidOperationException($"Type {typeof(T).FullName} is not enum type.");
            try
            {
                var t = new Maybe<T>(ObsTypeCaster.CastStruct<T>(str.Trim()));

                return ObsTypeCaster.CastStruct<T>(str);
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}