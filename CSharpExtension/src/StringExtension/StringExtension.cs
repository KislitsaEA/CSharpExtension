﻿using System;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace CSharpExtension.StringExtension
{
    public static class StringExtension
    {
        /// <summary>
        /// Разбивает строку по строкам
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string[] SplitByLines(this string value)
        {
            return Regex.Split(value, "\r\n|\r|\n");
        }


        public static bool IsNullOrEmpty(this string str)
        {
            return string.IsNullOrEmpty(str);
        }

        /// <summary>
        /// Исключает пустые строки из строки
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string TrimEmptyLines(this string value)
        {
            var lines = value.SplitByLines().Where(w => !string.IsNullOrEmpty(w)).ToArray();
            return string.Join(Environment.NewLine, lines);
            //var sb = new StringBuilder();
            //for (int i = 0; i < lines.Length; i++)
            //{
            //    if (i < lines.Length - 1)
            //        sb.AppendLine(lines[i]);
            //    else
            //        sb.Append(lines[i]);
            //}
            //return sb.ToString();
        }

        /// <summary>
        /// Добавляет точку в конец строки если её там нет
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string AddDotIfNeed(this string value)
        {
            value = value.TrimEnd();
            if (!value.EndsWith("."))
                value += ".";
            return value;
        }


        [Obsolete]
        public static int? AsInteger(this string value)
        {
            int i;
            if (int.TryParse(value, out i))
                return i;
            return null;
        }

        [Obsolete]
        public static double? AsFloat(this string value)
        {
            double i;
            if (double.TryParse(value, out i))
                return i;
            if (double.TryParse(value.Replace(',','.'), NumberStyles.Any, CultureInfo.GetCultureInfo("en-US"), out i))
                return i;
            return null;
        }

        [Obsolete]
        public static bool? AsBool(this string value)
        {
            bool i;
            if (bool.TryParse(value, out i))
                return i;
            return null;
        }


        /// <summary>
        /// Выполняет парсинг исходной сторки в Int32
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        [Obsolete]
        public static int? TryParseInt(this string str)
        {
            int i;
            if (int.TryParse(str, out i))
                return i;
            return null;
        }    
    }
}