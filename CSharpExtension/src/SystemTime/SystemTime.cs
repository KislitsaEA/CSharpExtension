using System;

namespace CSharpExtension.SystemTime
{
    /// <summary>
    /// ����������� ����� ��� ������ � ��������� ��������
    /// </summary>
    public static class SystemTime
    {
        #region System
        private static ITimeProvider _provider;

        static SystemTime()
        {
            Reset();    
        }

        /// <summary>
        /// ���������� ������ � �������� ���������.
        /// </summary>
        public static void Reset()
        {
            _provider = new TimeProvider();
        }

        /// <summary>
        /// ��������� ��������� ���������� ���������� �������. 
        /// �������� !!! �� ����������� ������ ����� � ����������, �� ������������ ��� ������������� � ������
        ///              ����� ���������� ����� �� �������� ������� ������ � �������� ��������� ������� ������ Reset()
        /// </summary>
        /// <param name="provider"></param>
        public static void Set(ITimeProvider provider)
        {
            _provider = provider;
        }
        #endregion

        public static DateTime Now => _provider.Now;
        public static DateTime NowUtc => _provider.UtcNow;
        public static DateTime Today => _provider.Today;
    }
}