using System;

namespace CSharpExtension.SystemTime
{
    public class FackeTimeProvider : ITimeProvider
    {
        public FackeTimeProvider():this(DateTime.Now) {}

        public FackeTimeProvider(DateTime now)
        {
            Now = now;
        }
        
        public DateTime Now { get; set; }
        public DateTime UtcNow => Now.ToUniversalTime();
        public DateTime Today => Now.Date;
    }
}