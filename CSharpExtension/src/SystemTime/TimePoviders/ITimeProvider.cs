using System;

namespace CSharpExtension.SystemTime
{
    public interface ITimeProvider{
        DateTime Now { get; }
        DateTime UtcNow { get; }
        DateTime Today { get; }
    }
}