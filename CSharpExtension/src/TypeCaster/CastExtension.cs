using System;

namespace CSharpExtension
{
    public static class CastExtension
    {
        public static T CastTo<T>(this object o)
        {
            return NewTypeCaster.Cast<T>(o);
        }

        public static T CastTo<T>(this object o, T defaultValue)
        {
            return NewTypeCaster.CastOrDefault<T>(o, defaultValue);
        }

        [Obsolete("use CastTo(defaultInstance)")]
        public static T CastOrDefault<T>(this object o, T defaultInstance)
        {
            return NewTypeCaster.CastOrDefault<T>(o, defaultInstance);
        }

        public static bool TryCast<T>(this object o, out T castValuer)
        {
            return NewTypeCaster.TryCast<T>(o, out castValuer);
        }
    }
}