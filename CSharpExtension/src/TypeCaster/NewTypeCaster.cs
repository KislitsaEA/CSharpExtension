using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace CSharpExtension
{
    public class NewTypeCaster
    {
        public static T Cast<T>(object o)
        {
            var type = typeof(T);
            if (type.IsEnum)
                return CastToEnum<T>(o);

            if ((type == typeof(double) || type == typeof(float)) && o is string str)
            {
                if (TryParseDouble(str, out var doubleValue))
                    return (T)Convert.ChangeType(doubleValue, type);
                throw new InvalidCastException();
            }

            if (IsConvertibleType<T>())
                return (T) Convert.ChangeType(o, type);

            return (T) (dynamic) o;
        }

        public static T CastOrDefault<T>(object value, T defaultInstance)
        {
            try { return Cast<T>(value); }
            catch (Exception) { return defaultInstance; }
        }

        public static bool TryCast<T>(object value, out T castValue)
        {
            try
            {
                castValue = Cast<T>(value);
                return true;
            }
            catch (Exception)
            {
                castValue = default(T);
                return false;
            }
        }

        #region enum
        private static T CastToEnum<T>(object value)
        {
            if (value is string s)
                return (T)Enum.Parse(typeof(T), s);
            return (T)value;
        }
        #endregion

        #region parse double
        public static bool TryParseDouble(string value, out double result)
        {
            var output = value.Trim().Replace(" ", "").Replace(",", ".");
            var split = output.Split('.');
            if (split.Length > 1)
            {
                output = string.Join("", split.Take(split.Length - 1).ToArray());
                output = $"{output}.{split.Last()}";
            }
            return double.TryParse(output, NumberStyles.Float, CultureInfo.InvariantCulture, out result);
        }

        #endregion

        #region ConvertibleTypes
        private static bool IsConvertibleType<T>() => IsConvertibleType(typeof(T));
        
        private static bool IsConvertibleType(Type type)
        {
            return ConvertibleTypes.Contains(type.FullName);
        }

        static NewTypeCaster()
        {
            var types = new[]
            {
                typeof(string),
                typeof(UInt64),
                typeof(UInt32),
                typeof(UInt16),
                typeof(Int64),
                typeof(Int32),
                typeof(Int16),
                typeof(Decimal),
                typeof(Double),
                typeof(Single),
                typeof(Char),
                typeof(Byte),
                typeof(SByte),
                typeof(Boolean),
            };

            ConvertibleTypes = new HashSet<string>(types.Select(s => s.FullName));
        }

        private static readonly HashSet<string> ConvertibleTypes;
        #endregion
    }
}