﻿using System;
using System.Globalization;

namespace CSharpExtension
{
    [Obsolete("Use NewTypeCaster")]
    public class ObsTypeCaster
    {
        public static bool TryParseDouble(string str, out double d)
        {
            if (double.TryParse(str, NumberStyles.Any, CultureInfo.CurrentCulture, out d)) return true;
            if (double.TryParse(str, NumberStyles.Any, CultureInfo.InvariantCulture, out d)) return true;
            return false;
        }

        public static T CastClass<T>(object o) where T : class
        {
            var obj = o as T;
            return obj;
        }

        public static T CastClass<T>(object o, T defaultInst) where T : class
        {
            return CastClass<T>(o) ?? defaultInst;
        }

        public static T CastStruct<T>(object o) where T : struct
        {
            var type = typeof (T);
            //Если в нём такого значения не определено
            if (type.IsEnum)
            {
                var s = o as string;
                if (s != null)
                {
                    try
                    {
                        return (T)Enum.Parse(typeof(T), s);
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                }
                if (NumberHelper.IsNumber(o))
                {
                    o = Cast<int>(o);
                }
                if (!type.IsEnumDefined(o)) throw new InvalidCastException();
            }
            var obj = (T) o;
            return obj;
        }

        public static T CastStruct<T>(object o, T defaultInst) where T : struct
        {
            if (o == null) return defaultInst;
            try
            {
                var t = CastStruct<T>(o);
                return t;
            }
            catch (InvalidCastException ex)
            {
                return defaultInst;
            }
        }

        public static T Cast<T>(object o)
        {
            if (typeof (T) == typeof (double) && o is string)
            {
                double d;
                if (TryParseDouble((string) o,out d)) return (T)(object)d;
            }
            return (T)Convert.ChangeType(o, typeof(T));
        }

        public static T Cast<T>(object o, IFormatProvider provider)
        {
            return provider != null
                ? (T) Convert.ChangeType(o, typeof (T), provider)
                : (T) Convert.ChangeType(o, typeof (T));
        }
        
        public static T Cast<T>(object o, T defaultInst)
        {
            try
            {
                try
                {
                    return Cast<T>(o);   
                }
                catch (FormatException)
                {
                    return Cast<T>(o, CultureInfo.InvariantCulture);
                }
            }
            catch (Exception ex)
            {
                return defaultInst;
            }

        }
    }
}
