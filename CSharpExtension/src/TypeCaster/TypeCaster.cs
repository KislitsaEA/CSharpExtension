using System;
using System.Globalization;
using System.Linq;

namespace CSharpExtension
{
    [Obsolete("Use NewTypeCaster")]
    public static class TypeCaster
    {
        public class RequireStruct<T> where T : struct { }
        public class RequireClass<T> where T : class { }

        public static bool TryCast<T>(object value,out T inst, RequireStruct<T> ignore = null) where T : struct
        {
            try
            {
                inst = Cast(value, ignore);
                return true;
            }
            catch (Exception)
            {
                inst = default(T);
                return false;
            }
        }

        public static T CastOrDefault<T>(object value, T defInst, RequireStruct<T> ignore = null) where T : struct
        {
            try { return Cast(value, ignore); }
            catch (Exception) { return defInst; }
        }

        public static T Cast<T>(object value, RequireStruct<T> ignore = null) where T : struct
        {
            var type = typeof(T);
            if (type.IsEnum)
                return CastToEnum<T>(value);
            if ((type == typeof (double) || type == typeof(float)) && value is string)
            {
                double d;
                if (TryParseDouble((string)value, out d))
                    return Cast<T>(d);
            }
            return (T)Convert.ChangeType(value, typeof(T));
        }

        public static bool TryCast<T>(object value, out T inst, RequireClass<T> ignore = null) where T : class
        {
            try
            {
                inst = Cast(value, ignore);
                return true;
            }
            catch (Exception)
            {
                inst = default(T);
                return false;
            }
        }

        public static T CastOrDefault<T>(object value, T defInst, RequireClass<T> ignore = null) where T : class
        {
            try { return Cast(value, ignore); }
            catch (Exception)  {return defInst;}
        }

        public static T Cast<T>(object value, RequireClass<T> ignore = null) where T : class
        {
            if (typeof (T) == typeof (string) && !(value is string))
                return Cast<T>(value.ToString());
            return value as T;
        }
        
        /// <summary>
        /// Выполняет преобразование в перечисление
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        private static T CastToEnum<T>(object value)
        {
            var s = value as string;
            if (s != null)
            {
                return (T)Enum.Parse(typeof(T), s);
            }
            return (T)value;
        }

        public static bool TryParseDouble(string value, out double result)
        {
            var output = value.Trim().Replace(" ", "").Replace(",", ".");
            string[] split = output.Split('.');
            if (split.Length > 1)
            {
                output = string.Join("", split.Take(split.Length - 1).ToArray());
                output = $"{output}.{split.Last()}";
            }
            if (double.TryParse(output, NumberStyles.Float, CultureInfo.InvariantCulture, out result))
                return true;
            return false;
        }

        //public static T CastTo<T>(object value)
        //{
        //    var type = typeof(T);
        //    switch (type)
        //    {
        //        case Type t when t.IsValueType:
        //        {
        //            return (T)value;
        //            break;
        //        }
        //        case Type t when T: class:
        //        {
        //            return (T)value;
        //            break;
        //        }
        //        default:
        //        {
        //            return value as T;
        //            break;
        //        }
        //    }
        //    throw new NotImplementedException();
        //}
    }
}